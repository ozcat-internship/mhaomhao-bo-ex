const {
  override,
  fixBabelImports,
  addWebpackAlias,
  addLessLoader,
  addDecoratorsLegacy

} = require('customize-cra')
const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
// module.exports = function override (config, env) {
//   config.resolve = {
//     alias: {
//       themes: path.resolve(__dirname, 'src/themes'),
//       pages: path.resolve(__dirname, 'src/pages'),
//       routes: path.resolve(__dirname, 'src/routes'),
//       '@': resolve('src'),
//     }
//   }
//   config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config)
//   config = rewireLess(config, env, {
//     javascriptEnabled: true
//   })
//   config = rewireMobX(config, env)
//   return config
// }

module.exports = {
  webpack: override(
    fixBabelImports('import', {
      libraryName: 'antd', libraryDirectory: 'es', style: true // change importing css to less
    }),
    addWebpackAlias({
      themes: path.resolve(__dirname, 'src/themes'),
      pages: path.resolve(__dirname, 'src/pages'),
      routes: path.resolve(__dirname, 'src/routes'),
      '@': resolve('src'),
    }),
    addLessLoader({
      javascriptEnabled: true
    }),
    addDecoratorsLegacy()
  )
}
