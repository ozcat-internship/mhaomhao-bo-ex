import React, { Component } from 'react'
import MainRoute from '@/routes'
import './App.scss'
class App extends Component {
  render () {
    return (
      <MainRoute />
    )
  }
}

export default App
