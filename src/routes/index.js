import React, { Component } from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import DefaultLayout from '@/layouts/Default'
export default class MainRoute extends Component {
  render () {
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/' name='Home' component={DefaultLayout} />
        </Switch>
      </BrowserRouter>
    )
  }
}
