import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Divider, Tag, Button, Input, Select } from 'antd'
const Search = Input.Search
const Option = Select.Option
const InputGroup = Input.Group

const options = [{
  value: 'zhejiang',
  label: 'Zhejiang',
  children: [{
    value: 'hangzhou',
    label: 'Hangzhou',
    children: [{
      value: 'xihu',
      label: 'West Lake',
    }],
  }],
}, {
  value: 'jiangsu',
  label: 'Jiangsu',
  children: [{
    value: 'nanjing',
    label: 'Nanjing',
    children: [{
      value: 'zhonghuamen',
      label: 'Zhong Hua Men',
    }],
  }],
}]

const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
  render: text => <a href='javascript:;'>{text}</a>,
}, {
  title: 'Age',
  dataIndex: 'age',
  key: 'age',
}, {
  title: 'Address',
  dataIndex: 'address',
  key: 'address',
}, {
  title: 'Tags',
  key: 'tags',
  dataIndex: 'tags',
  render: tags => (
    <span>
      {tags.map(tag => <Tag color='blue' key={tag}>{tag}</Tag>)}
    </span>
  ),
}, {
  title: 'Action',
  key: 'action',
  render: (text, record) => (
    <span>
      <a href='javascript:;'>Approve Gardener</a>
      <Divider type='vertical' />
      <a href='javascript:;'>Approve Merchant</a>
    </span>
  ),
}]

const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
  tags: ['nice', 'developer'],
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  address: 'London No. 1 Lake Park',
  tags: ['loser'],
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  address: 'Sidney No. 1 Lake Park',
  tags: ['cool', 'teacher'],
}]

export default class CreditWithdrawPage extends Component {
  static propTypes = {
    prop: PropTypes
  }

  render () {
    return (
      <div>
        <div style={{
          marginBottom: 16
        }}>
          <InputGroup compact style={{ display: 'flex' }}>
            <Select defaultValue='Sign Up' style={{ flex: 0 }}>
              <Option value='Sign Up'>Sign Up</Option>
              <Option value='Sign In'>Sign In</Option>
            </Select>
            <Search
              style={{ flex: 1 }}
              placeholder='input search text'
              enterButton='Search'
              onSearch={value => console.log(value)}
            />
          </InputGroup>
        </div>
        <Table dataSource={data} columns={columns} />
      </div>
    )
  }
}
