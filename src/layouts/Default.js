import React from 'react'
import { Layout, Menu, Icon } from 'antd'
import { withRouter, Route, Switch, BrowserRouter, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import SpeciesPage from '@/pages/SpeciesPage/SpeciesPage'
import SubSpeciesPage from '@/pages/SubSpeciesPage/SubSpeciesPage'
import MutateSpeciesPage from '@/pages/SpeciesPage/MutateSpeciesPage'
import MutateSubSpeciesPage from '@/pages/SubSpeciesPage/MutateSubSpeciesPage'
import UserPage from '@/pages/UserPage/UserPage'
import TransferPage from '@/pages/TransferPage/TransferPage'
import CreditWithdrawPage from '@/pages/CreditWithdrawPage/CreditWithdrawPage'
import AuctionItemPage from '@/pages/AuctionItemPage/AuctionItemPage'
import AppointmentPage from '@/pages/InboxPage/AppointmentPage'
import ApproveGardenerPage from '@/pages/InboxPage/ApproveGardenerPage'
import ApproveMerchantPage from '@/pages/InboxPage/ApproveMerchantPage'
import CompleteTransferPage from '@/pages/InboxPage/CompleteTransferPage'
import NotificationFeePage from '@/pages/InboxPage/NotificationFeePage'
const { Header, Sider, Content } = Layout
const SubMenu = Menu.SubMenu
class Default extends React.Component {
  static propTypes = {
    history: PropTypes.object
  }
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    })
  }

  render () {
    console.log('-------', this.props)
    return (
      <Layout style={{ minHeight: '100%' }}>
        <Sider
          style={{
            overflow: 'auto', height: '100vh', position: 'fixed', zIndex: 999
          }}
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          width={250}
        >
          <div className='logo' />
          <Menu theme='dark' mode='inline' defaultSelectedKeys={['1']}>
            <Menu.Item key='1' onClick={() => this.props.history.push('/species')}>
              <Icon type='file' />
              <span>Species</span>
            </Menu.Item>
            <Menu.Item key='2' onClick={() => this.props.history.push('/subspecies')}>
              <Icon type='file-text' />
              <span>SubSpecies</span>
            </Menu.Item>
            <Menu.Item key='3' onClick={() => this.props.history.push('/user')}>
              <Icon type='user' />
              <span>User</span>
            </Menu.Item>
            <SubMenu key='4' title={<span><Icon type='inbox' /><span>Inbox</span></span>}>
              <Menu.Item key='9' onClick={() => this.props.history.push('/inbox/approve-merchant')}>
                <Icon type='inbox' />
                <span>Approve Merchant</span>
              </Menu.Item>
              <Menu.Item key='10' onClick={() => this.props.history.push('/inbox/approve-gardener')}>
                <Icon type='inbox' />
                <span>Approve Gardener</span>
              </Menu.Item>
              <Menu.Item key='11' onClick={() => this.props.history.push('/inbox/notification-fee')}>
                <Icon type='inbox' />
                <span>Notification Fee</span>
              </Menu.Item>
              <Menu.Item key='12' onClick={() => this.props.history.push('/inbox/complete-transfer')}>
                <Icon type='inbox' />
                <span>Complete Transfer</span>
              </Menu.Item>
              <Menu.Item key='13' onClick={() => this.props.history.push('/inbox/appointment')}>
                <Icon type='inbox' />
                <span>Appointment Product</span>
              </Menu.Item>
            </SubMenu>
            <Menu.Item key='5' onClick={() => this.props.history.push('/transfer')}>
              <Icon type='file-done' />
              <span>Transfer</span>
            </Menu.Item>
            <Menu.Item key='6' onClick={() => this.props.history.push('/credit-withdraw')}>
              <Icon type='credit-card' />
              <span>Credit Withdraw</span>
            </Menu.Item>
            <Menu.Item key='7' onClick={() => this.props.history.push('/auction-item')}>
              <Icon type='book' />
              <span>Auction</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout style={{ marginLeft: this.state.collapsed ? 80 : 250 }}>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              style={{ marginLeft: 16 }}
              className='trigger'
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content style={{
            margin: '24px 16px', padding: 24, background: '#fff', height: '100%',
          }}
          >
            <Switch>
              <Route exact path='/species' component={SpeciesPage} />
              <Route exact path='/subspecies' component={SubSpeciesPage} />
              <Route exact path='/species/create' component={MutateSpeciesPage} />
              <Route exact path='/subspecies/create' component={MutateSubSpeciesPage} />
              <Route exact path='/user' component={UserPage} />
              <Route exact path='/transfer' component={TransferPage} />
              <Route exact path='/credit-withdraw' component={CreditWithdrawPage} />
              <Route exact path='/auction-item' component={AuctionItemPage} />
              <Route exact path='/inbox/appointment' component={AppointmentPage} />
              <Route exact path='/inbox/approve-gardener' component={ApproveGardenerPage} />
              <Route exact path='/inbox/approve-merchant' component={ApproveMerchantPage} />
              <Route exact path='/inbox/complete-transfer' component={CompleteTransferPage} />
              <Route exact path='/inbox/notification-fee' component={NotificationFeePage} />
              <Redirect from='/' to='/species' />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    )
  }
}
export default withRouter(Default)
